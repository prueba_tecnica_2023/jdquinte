drop table if exists proceso_vspc_medios_de_pago.obligaciones_clientes_final purge;
CREATE table proceso_vspc_medios_de_pago.obligaciones_clientes_final stored as PARQUET as

WITH base_tasa AS
(
	SELECT
	t1.radicado, t1.num_documento, t1.cod_segm_tasa, t1.cod_subsegm_tasa, t1.cal_interna_tasa,
	t1.id_producto, t1.tipo_id_producto, t1.valor_inicial, t1.fecha_desembolso, t1.plazo,
	t1.cod_periodicidad, t1.periodicidad, t1.saldo_deuda, t1.modalidad, t1.tipo_plazo,
	CASE
	WHEN right(t1.id_producto,20) = 'operacion_especifica' THEN t2.tasa_operacion_especifica
	WHEN right(t1.id_producto,13) = 'Cartera Total' THEN t2.tasa_cartera
	WHEN right(t1.id_producto,7) = 'cartera' THEN t2.tasa_cartera
	WHEN right(t1.id_producto,7) = 'leasing' THEN t2.tasa_leasing
	WHEN right(t1.id_producto,4) = 'Sufi' THEN t2.tasa_sufi
	WHEN right(t1.id_producto,9) = 'factoring' THEN t2.tasa_factoring
	WHEN right(t1.id_producto,11) = 'Hipotecario' THEN t2.tasa_hipotecario
	ELSE t2.tasa_tarjeta 
	END tasa
	FROM proceso_vspc_medios_de_pago.obligaciones_clientes AS t1
	INNER JOIN proceso_vspc_medios_de_pago.tasas_productos AS t2 on
	t1.cod_segm_tasa = t2.cod_segmento and t1.cod_subsegm_tasa = t2.cod_subsegmento
	and t1.cal_interna_tasa = t2.calificacion_riesgos
),
base_efectiva AS
(
	SELECT
	t1.radicado, t1.num_documento, t1.cod_segm_tasa, t1.cod_subsegm_tasa, t1.cal_interna_tasa,
	t1.id_producto, t1.tipo_id_producto, t1.valor_inicial, t1.fecha_desembolso, t1.plazo,
	t1.cod_periodicidad, t1.periodicidad, t1.saldo_deuda, t1.modalidad, t1.tipo_plazo,
	t1.tasa, power((1+t1.tasa),(1/(12/t1.cod_periodicidad))) - 1 AS tasa_efectiva
	FROM base_tasa AS t1
)
	SELECT
	t1.radicado, t1.num_documento, t1.cod_segm_tasa, t1.cod_subsegm_tasa, t1.cal_interna_tasa,
	t1.id_producto, t1.tipo_id_producto, t1.valor_inicial, t1.fecha_desembolso, t1.plazo,
	t1.cod_periodicidad, t1.periodicidad, t1.saldo_deuda, t1.modalidad, t1.tipo_plazo,
	t1.tasa, t1.tasa_efectiva, t1.tasa_efectiva * t1.valor_inicial AS valor_final
	FROM base_efectiva AS t1
;

drop table if exists proceso_vspc_medios_de_pago.obligaciones_clientes_unico purge;
CREATE table proceso_vspc_medios_de_pago.obligaciones_clientes_unico stored as PARQUET as
SELECT
t1.num_documento, sum(t1.valor_final) valor_final
FROM proceso_vspc_medios_de_pago.obligaciones_clientes_final AS t1
group by t1.num_documento
having sum(t1.valor_final) > 0;