def promedio(list):
	"""
	Calcula el promedio de un secuencia de numeros.
	Args:
		list: Lista de numeros
	Returns:
		número con el promedio de la secuencia
	"""
	n = len(list)
	suma = sum(list)
	promedio = suma / n
	return promedio

def promedio_mayor ():
	"""
	Lee una secuencia de números por teclado y calcula el
	promedio de los valores mayores al promedio de la secuencia.
	Returns:
		número con el resultado el promedio
	"""
	numero = input("Por favor ingrese un número: ")
	list = []
	while numero != '-1':
		if numero.isnumeric():
			list.append(int(numero))
		numero = input("Por favor ingrese un número: ")
	
	prom = promedio(list)
	list2 = [x for x in list if x > prom]
	prom2 = promedio(list2)
	
	return prom2

def suma_cercana (list, x):
	"""
	Calcula el par de números, cuya suma es más cercana a valor de x.
	Args:
		list: Lista de numeros
		x: nuemro para carcular el par
	Returns:
		Par números con la suma más cercana a x.
	"""
	par_1 = None
	par_2 = None
	for i in range(len(list)):
		par_i = list[i]
		j = i + 1
		while j < len(list):
			par_j = list[j]
			suma_par = par_i + par_j
			if par_1 is None or par_2 is None or abs(suma_par - x) < abs(par_1 + par_2 - x):
				par_1 = par_i
				par_2 = par_j
			j += 1
	return par_1, par_2

def rotar_horario (matriz, m, n, ciclos, cuadrados):
	"""
	Rota una marix en sentido horario.
	Args:
		matriz: Matriz de dimensión m x n
		m: El número de filas
		n: El número de columas
		ciclos: Número debe veces que se debe rotar
		cuadrados: Número de cuadrados dentro de la matriz
	Returns:
		Marix rotada.
	"""
	for r in range(ciclos):
		for c in range(cuadrados):
			i = c
			j = c
			nuevo_valor = matriz[i][j]
			
			while j < n - c - 1:	
				anterior = matriz[i][j+1]
				matriz[i][j+1] = nuevo_valor
				nuevo_valor = anterior
				j += 1
			
			while i < m - c - 1:
				anterior = matriz[i+1][j]
				matriz[i+1][j] = nuevo_valor
				nuevo_valor = anterior
				i += 1
			
			while j >  c :	
				anterior = matriz[i][j-1]
				matriz[i][j-1] = nuevo_valor
				nuevo_valor = anterior
				j -= 1
			
			while i > c:	
				anterior = matriz[i-1][j]
				matriz[i-1][j] = nuevo_valor
				nuevo_valor = anterior
				i -= 1			

	return matriz

def rotar_antihorario (matriz, m, n, ciclos, cuadrados):
	"""
	Rota una marix en sentido antihorario.
	Args:
		matriz: Matriz de dimensión m x n
		m: El número de filas
		n: El número de columas
		ciclos: Número debe veces que se debe rotar
		cuadrados: Número de cuadrados dentro de la matriz
	Returns:
		Marix rotada.
	"""
	for r in range(ciclos):
		for c in range(cuadrados):
			i = c
			j = c
			nuevo_valor = matriz[i][j]
			while i < m - c - 1:
				anterior = matriz[i+1][j]
				matriz[i+1][j] = nuevo_valor
				nuevo_valor = anterior
				i += 1
			
			while j < n - c - 1:	
				anterior = matriz[i][j+1]
				matriz[i][j+1] = nuevo_valor
				nuevo_valor = anterior
				j += 1
			
			while i > c:	
				anterior = matriz[i-1][j]
				matriz[i-1][j] = nuevo_valor
				nuevo_valor = anterior
				i -= 1
			
			while j >  c :	
				anterior = matriz[i][j-1]
				matriz[i][j-1] = nuevo_valor
				nuevo_valor = anterior
				j -= 1			

	return matriz

def rotar_matriz (matriz, p):
	"""
	Rota una marix p veces. La rotación en sentido antihorario
	si p < 0 y en sentido horario cuando p > 0.
	Args:
		matriz: Matriz de dimensión m x n
		p: nuemro debe veces que se debe rotar
	Returns:
		Marix rotada.
	"""
	m = len(matriz)
	n = len(matriz[0])
	minimo = min(m, n)
	if m >= 2 and n <= 100 and abs(p) >= 1 and abs(p) <= 10000000 and minimo%2 == 0:
		#Calcular el número de cuadrados dentro de la matriz
		cuadrados = int(minimo/2)	
		if minimo%2 > 0:
			cuadrados = cuadrados + 1
		if p < 0:
			matriz = rotar_antihorario (matriz, m, n, abs(p), cuadrados)
		else:
			matriz = rotar_horario (matriz, m, n, abs(p), cuadrados)
	
	return matriz


def main():
	#Solucion del punto 3.4 numeral a)
	resultado = promedio_mayor()
	
	#Solucion del punto 3.4 numeral b)
	list = [10, 22, 28, 29, 30, 40]
	x = 54
	par_1, par_2 = suma_cercana (list,x)
	print(par_1)
	print(par_2)
	
	
	#Solucion del punto 3.4 numeral c)
	matriz = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16],[17, 18, 19, 20]]
	p = 2
	matriz_r = rotar_matriz(matriz,p)
	print(matriz_r)


if __name__ == "__main__":
	main()
