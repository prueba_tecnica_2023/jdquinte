SELECT
t1.cedula, t1.nombre, t1.region, t1.edad
FROM proceso_vspc_medios_de_pago.cliente as t1
LEFT JOIN proceso_vspc_medios_de_pago.clave_dinamica AS t2
on t1.cedula = t2.cedula_cliente
where t2.cedula_cliente is null