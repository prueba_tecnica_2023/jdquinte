SELECT
sum(saldo) saldo_total
FROM proceso_vspc_medios_de_pago.cuentas as t3
inner join proceso_vspc_medios_de_pago.clave_dinamica AS t2
on t3.cedula_cliente = t2.cedula_cliente
inner join proceso_vspc_medios_de_pago.cliente AS t1
on t2.cedula_cliente = t1.cedula
where t3.fecha_apertura >= 20180501 and t3.fecha_apertura < 20180601