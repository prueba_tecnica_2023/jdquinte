SELECT
t1.cedula, count(*) cuentas_activas
FROM proceso_vspc_medios_de_pago.cuentas as t2
inner join proceso_vspc_medios_de_pago.cliente AS t1
on t2.cedula_cliente = t1.cedula
where t2.estado = 1
group by t1.cedula
having count(*) > 3